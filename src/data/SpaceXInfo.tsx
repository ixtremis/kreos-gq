interface SpaceXInfo {
    company: {
      ceo: string;
      coo: string;
      cto: string;
      cto_propulsion: string;
      employees: number;
      founded: number;
      founder: string;
      launch_sites: number;
      name: string;
      summary: string;
      test_sites: number;
      valuation: number;
      vehicles: number;
    };
  }