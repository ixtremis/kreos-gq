
import { Title, Paragraph, Text } from "react-native-paper";
import { createGraphQlClient } from "../../modules/kreos-module";

const ErrorComponent = (error: any) => <Text>Error: {error.message}</Text>;

const LAUNCHES_QUERY = `
    query LaunchesLatest($limit: Int, $offset: Int) {
      launchesPast(limit: $limit, offset: $offset) {
        id,
        launch_date_utc
        mission_name
      }
    }
  `;

const LAUNCH_DETAILS_QUERY = `
    query LaunchDetails($id: ID!) {
      launch(id: $id) {
        details
        launch_date_utc
        launch_success
        mission_name
        mission_id
        links {
          article_link
          video_link
          wikipedia
        }
        rocket {
          rocket_name
          rocket_type
          rocket {
              id
              name
          }
        } 
      }
    }
  `;

  const COMPANY_QUERY = `
  query Company {
    company {
        ceo
        coo
        cto
        cto_propulsion
        employees
        founded
        founder
        launch_sites
        name
        summary
        test_sites
        valuation
        vehicles
    }
}
  `

const ROCKET_QUERY = `
  query Rocket($id: ID!) {
    rocket(id: $id) {
        active
        boosters
        company
        cost_per_launch
        country
        description
        first_flight
        id
        name
        stages
        success_rate_pct
        type
        wikipedia
    }
  }
`

const MISSION_QUERY = `
  query Mission($id: ID!) {
    mission(id: $id) {
      description
      id
      manufacturers
      name
      twitter
      website
      wikipedia
  }
  }
`


const GRAPHQL_CLIENT = createGraphQlClient('https://spacex-production.up.railway.app/');
GRAPHQL_CLIENT.setupCache(MISSION_QUERY, 5);
GRAPHQL_CLIENT.setupCache(ROCKET_QUERY, 15);

export {ErrorComponent, LAUNCHES_QUERY, LAUNCH_DETAILS_QUERY, GRAPHQL_CLIENT, COMPANY_QUERY, ROCKET_QUERY, MISSION_QUERY}