interface Launch {
    id: string;
    mission_name: string;
    launch_date_utc: string;
    details: string;
}

interface Launches {
  launchesPast: Launch[];
}

interface Links {
  article_link: string
  video_link: string
  wikipedia: string
}

interface RocketData {
  id: string
}

interface RocketDetails {
  rocket_name: string
  rocket_type: string
  rocket: RocketData
}

interface LaunchDetails {
  id: string;
  mission_name: string;
  mission_id: string[];
  launch_date_utc: string;
  details: string;
  launch_success: Boolean;
  links: Links;
  rocket: RocketDetails

}

interface LaunchData {
  launch: LaunchDetails
}
  
export { Launch, Launches, LaunchData, Links, LaunchDetails };