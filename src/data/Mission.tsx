interface Mission {
    mission: MissionData | undefined
}

interface MissionData {
    description: string
    manufacturers: string
    id: string
    name: string
    twitter: string
    website: string
    wikipedia: string
}