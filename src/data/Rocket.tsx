interface Rocket {
    rocket: RocketData
}

interface RocketData {
    active: boolean
    boosters: number
    company: string
    cost_per_launch: number
    country: string
    description: string
    first_flight: string
    id: string
    name: string
    stages: string
    success_rate_pct: string
    type: string
    wikipedia: string
}