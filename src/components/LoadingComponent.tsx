import { ActivityIndicator } from 'react-native-paper';
import { View, Text, StyleSheet } from "react-native"; 

const LoadingComponent = () => {
    return (
      <View style={styles.container}>
        <View style={styles.centered}>
          <ActivityIndicator animating={true} color='#800080' size='large' />
        </View>
      </View>
    );
  };
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
    },
    centered: {
      justifyContent: 'center',
      alignItems: 'center',
      paddingTop: 150
    },
  });
  
  export default LoadingComponent;