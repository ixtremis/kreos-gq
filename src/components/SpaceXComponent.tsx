import React, { useEffect, useState } from 'react';
import LaunchTable from './LaunchTable';
import { Button } from 'react-native-paper';
import { GRAPHQL_CLIENT, LAUNCHES_QUERY } from '../data/Constants';
import { Launch, Launches } from '../data/Launches';
import { View } from 'react-native';


const SpaceXComponent = () => {

  const [page, setPage] = useState<number>(1);
  const [launches, setLaunches] = useState<Launch[]>([]);
  const [loading, setLoading] = useState<boolean>(false);

  const { loading: gqlLoading, data: gqlData, error: gqlError, execute } = GRAPHQL_CLIENT.useGraphQLQuery<Launches>(
    LAUNCHES_QUERY,
    new Map<string, any>([['limit', 12], ['offset', (page - 1) * 12]])
  );

  useEffect(() => {
    setLoading(gqlLoading);

    if (gqlData && gqlData.launchesPast) {
      setLaunches(gqlData.launchesPast);
    }
  }, [gqlLoading, gqlData]);

  const handleLoadMore = async () => {
    setPage((prevPage) => prevPage + 1);
  };

  const handleLoadLess = async () => {
    if (page > 1) {
      setPage((prevPage) => prevPage - 1);
    }
  };

  return (
    <View>
      <LaunchTable data={gqlData?.launchesPast || []}/>
      <View style={{display: "flex", justifyContent: "space-between", flexDirection: 'row', gap: 2, margin: 2}} >
        <Button mode="contained" onPress={handleLoadLess} disabled={loading || page === 1}  style={{ flex: 1 }}>
          Previous
        </Button>
        <Button mode="contained" onPress={handleLoadMore} disabled={loading} style={{ flex: 1 }}>
        Next
        </Button>
      </View>
    </View>
  );
};

export default SpaceXComponent;
