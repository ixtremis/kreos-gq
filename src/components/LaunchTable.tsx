import React from 'react';
import { DataTable } from 'react-native-paper';
import { Launch } from '../data/Launches';
import { ParamListBase, useNavigation } from '@react-navigation/native';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';


interface LaunchTableProps {
  data: Launch[];
}

const LaunchTable = ({ data }: LaunchTableProps) => {

  const navigation = useNavigation() as NativeStackNavigationProp<ParamListBase>;

  const handleLaunchClick = (launchId: string) => {
    navigation.navigate('AboutLaunch', { launchId });
  };
  
  return (
    <DataTable style={{backgroundColor: 'primary'}}>
      <DataTable.Header>
        <DataTable.Title>Mission name</DataTable.Title>
        <DataTable.Title>Launch date</DataTable.Title>
      </DataTable.Header>

      {data.map((item) => (
        <DataTable.Row key={item.id} onPress = {() => handleLaunchClick(item.id)}>
          {/* <DataTable.Cell>{item.id}</DataTable.Cell> */}
          <DataTable.Cell>{item.mission_name}</DataTable.Cell>
          <DataTable.Cell>{item.launch_date_utc}</DataTable.Cell>
        </DataTable.Row>
      ))}
    </DataTable>
  );
};

export default LaunchTable;