import React from 'react';
import { Linking, TextStyle } from 'react-native';
import { Text, TouchableRipple } from 'react-native-paper';

const styles = {
    linkText: {
      color: '#0097a7', 
      textDecorationLine: 'underline',  
      marginBottom: 10, 
    } as TextStyle,
  };

const DataLink = ({ label, url }: { label: string; url: string | undefined }) => {
    const openLink = () => {
      if (url) {
        Linking.openURL(url).catch((err) => console.error('An error occurred', err));
      }
    };
  
    return (
      <TouchableRipple onPress={openLink}>
        <Text style={styles.linkText}>{label}</Text>
      </TouchableRipple>
    );
  };

  export default DataLink;