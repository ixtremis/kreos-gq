import { View, TextStyle, TouchableOpacity } from "react-native";
import { Title, Paragraph, Text, Card, Divider   } from "react-native-paper";
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { LaunchData, LaunchDetails } from "../data/Launches";
import DataLink from "./DataLink";
import { ParamListBase } from "@react-navigation/native";
import { NativeStackNavigationProp } from "@react-navigation/native-stack";
import ReadMore from "./ReadMore";


const styles = {
    card: { margin: 10, padding: 15, height: '95%', backgroundColor: 'secondary' } as TextStyle,
    text: { marginBottom: 10  } as TextStyle,
  };
  

  

const LaunchDetailsComponent = (
    {launch, navigation}:
    { launch : LaunchDetails, navigation: NativeStackNavigationProp<ParamListBase>}
    ) => {

        const handleMissionMore = (missionId: string) => {
            navigation.navigate('AboutMission', { missionId });
          };


    const handleArrowPress = (rocketId: string) => {
        navigation.navigate('AboutRocket', { rocketId });
      };
    
    return (
        <Card style={styles.card}>
          <View style={{marginBottom: 5}}>
            <Title>Mission: {launch.mission_name}</Title>
            <Text style={styles.text}>Launch Date: {launch.launch_date_utc}</Text>
            <Text style={styles.text}>Description:</Text>
            <Paragraph>
                <Text style={styles.text}>{launch.details || 'Nothing provided'}</Text>
            </Paragraph>
            {/* {launch.mission_id.map(e => { return <ReadMore text='Read more about mission' onPress={() => handleMissionMore(e)}/>})} */}
          </View>
          <Divider bold/>
          <Title>Rocket Details:</Title>
          <Text style={styles.text}>Name: {launch.rocket.rocket_name}</Text>
          <Text style={styles.text}>Type: {launch.rocket.rocket_type}</Text>
          <Text style={styles.text}>Type: {launch.rocket.rocket.id}</Text>
        <ReadMore text='Read more' onPress={() => handleArrowPress(launch.rocket.rocket.id)}/>
          <Divider bold/>
        <Title>All links:</Title>
        <DataLink label="Watch video" url={launch.links.video_link}/>
        <DataLink label="Visit Wikipedia" url={launch.links.wikipedia}/>
        <DataLink label="Visit Space.com article" url={launch.links.article_link}/>
        </Card>
      );
}

  export default LaunchDetailsComponent;