import { View } from "react-native";
import { Title, Paragraph, Text, Divider } from "react-native-paper";
import { GRAPHQL_CLIENT, ErrorComponent, ROCKET_QUERY, MISSION_QUERY } from "../data/Constants";
import LoadingComponent from "./LoadingComponent";
import { RouteProp, ParamListBase } from "@react-navigation/native";
import DataLink from "./DataLink";

interface AboutRocketProps {
  route: RouteProp<ParamListBase, "AboutMission"> & { params: { missionId?: string } }; 
}

const MissionInfoComponent =  ({ route }: AboutRocketProps) => {

  const { missionId = '' } = route.params || {};

  const DataComponent = ({mission}: Mission) => (
    mission ? (
      <View style={{ padding: 16 }}>
        <Title>{`Mission ${mission.name}`}</Title>
        <Text>Manufacturers: {mission.manufacturers}</Text>
        <Text>Description: </Text>
        <Paragraph>{mission.description}</Paragraph>
        <Divider bold />
        <View style={{ marginTop: 8 }}>
          <DataLink label="Visit Twitter" url={mission.twitter} />
          <DataLink label="Visit website" url={mission.website} />
          <DataLink label="Visit Wikipedia" url={mission.wikipedia} />
        </View>
      </View>
    ) : (
      <Text>No data</Text>
    )
  );

      
    const aboutMission = GRAPHQL_CLIENT.createComponent<Mission>(
        MISSION_QUERY, 
        new Map([['id', missionId]]),
        {
            loadingComponent: LoadingComponent,
            dataComponent: DataComponent,
            errorComponent: ErrorComponent
        }
    )
  
    return (
      <View>
        {aboutMission}
      </View>
    );
  };
  
  export default MissionInfoComponent;
  