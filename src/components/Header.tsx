import React from 'react';
import { Appbar } from 'react-native-paper';
import { NativeStackNavigationProp, createNativeStackNavigator } from '@react-navigation/native-stack';
import SpaceXInfoComponent from './SpaceXInfoComponent';
import SpaceXComponent from './SpaceXComponent';
import { NavigationContainer, ParamListBase, RouteProp, useRoute } from '@react-navigation/native';
import AboutLaunch from './AboutLaunch';
import RocketInfoComponent from './RocketInfoComponent';
import MissionInfoComponent from './MissionInfoComponent';

interface NavBarProps {
  navigation: NativeStackNavigationProp<ParamListBase, string, undefined>,
  back: {
    title: string;
} | undefined
}


export type RootStackParamList = {
  Home: undefined;
  Launches: undefined;
  AboutLaunch: { launchId?: string }; 
  AboutRocket: { rocketId?: string }; 
  AboutMission: { missionId?: string }; 
};

const Stack = createNativeStackNavigator<RootStackParamList>();

const NavBar = ({navigation, back}: NavBarProps) => {

  const route = useRoute<RouteProp<ParamListBase, string>>();

  return (
    <Appbar.Header style={{backgroundColor: '#392a47'}} dark>
       {back ? <Appbar.BackAction onPress={navigation.goBack} color='white'/> : null}
    <Appbar.Content title="SpaceX GraphQL API" />
    {route.name === 'Home' && (
        <Appbar.Action icon="rocket"  color='white' onPress={() => navigation.navigate('Launches')} />
      )}
</Appbar.Header>
  )
}

const Header = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Home" screenOptions={{
          header: (props) => <NavBar navigation={props.navigation} back={props.back}/>,
        }}>
        <Stack.Screen name="Home" component={SpaceXInfoComponent} />
        <Stack.Screen name="Launches" component={SpaceXComponent} />
        
        <Stack.Screen name="AboutLaunch" component={AboutLaunch} />
        <Stack.Screen name="AboutRocket" component={RocketInfoComponent} />
        <Stack.Screen name="AboutMission" component={MissionInfoComponent} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}; 

export default Header;

