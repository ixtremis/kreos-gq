import React from "react";
import { GRAPHQL_CLIENT, LAUNCH_DETAILS_QUERY, ErrorComponent } from "../data/Constants";
import { LaunchData } from "../data/Launches";
import { View, TextStyle } from "react-native";
import { RouteProp, ParamListBase, useNavigation } from "@react-navigation/native";
import LoadingComponent from "./LoadingComponent";
import { SafeAreaView } from "react-native";
import LaunchDetailsComponent from "./LaunchDetailsComponent";
import { NativeStackNavigationProp } from "@react-navigation/native-stack";

interface AboutLaunchProps {
  route: RouteProp<ParamListBase, 'AboutLaunch'> & { params: { launchId?: string } }; 
}

const AboutLaunch = ({ route }: AboutLaunchProps) => {

  const { launchId = '' } = route.params || {};

  const navigation = useNavigation<NativeStackNavigationProp<ParamListBase>>();

  

  const aboutLaunch = GRAPHQL_CLIENT.createComponent<LaunchData>(
    LAUNCH_DETAILS_QUERY, 
    new Map([['id', launchId]]),
    {
        loadingComponent: LoadingComponent,
        dataComponent: (data) => <LaunchDetailsComponent 
          launch={data.launch} 
          navigation={navigation}
        />,
        errorComponent: ErrorComponent
    }
)

  return (
    <SafeAreaView>
      <View style={{display: 'flex'}}>
       {aboutLaunch}
    </View>
    </SafeAreaView>
  );
};

export default AboutLaunch;

