import { View } from "react-native";
import { Title, Paragraph, Text, Divider } from "react-native-paper";
import { GRAPHQL_CLIENT, COMPANY_QUERY, ErrorComponent } from "../data/Constants";
import LoadingComponent from "./LoadingComponent";


const SpaceXInfoComponent = () => {

    const DataComponent =  (data: SpaceXInfo) => (
        <View style={{ padding: 16 }}>
          <Title>{data.company.name} goals:</Title>
          <Paragraph>{data.company.summary}</Paragraph>
          <Divider bold/>
          <View style={{ marginTop: 8 }}>
            <Text>CEO: {data.company.ceo}</Text>
            <Text>COO: {data.company.coo}</Text>
            <Text>CTO: {data.company.cto}</Text>
          </View>
        </View>
      )

      
    const aboutCompany = GRAPHQL_CLIENT.createComponent<SpaceXInfo>(
        COMPANY_QUERY, 
        new Map(),
        {
            loadingComponent: LoadingComponent,
            dataComponent: DataComponent,
            errorComponent: ErrorComponent
        }
    )
  
    return (
      <View>
        {aboutCompany}
      </View>
    );
  };
  
  export default SpaceXInfoComponent;
  