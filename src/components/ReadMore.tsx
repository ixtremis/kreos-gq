import React from "react";
import { GestureResponderEvent, TextStyle, TouchableOpacity, View } from "react-native";
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { Text } from "react-native-paper";

const styles = {
    text: { marginBottom: 10  } as TextStyle,
  };

const ReadMore = ({text, onPress}: {text: string, onPress?: ((event: GestureResponderEvent) => void) | undefined}) => {
    return (      
        <TouchableOpacity onPress={onPress}>
        <View style={{display: "flex", flexDirection: "row"}}>
        <Text style={styles.text}>{text}</Text>
        <MaterialCommunityIcons name="arrow-right" size={24} color="black" />
        </View>
      </TouchableOpacity>
    )
}

export default ReadMore;