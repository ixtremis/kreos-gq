import { View } from "react-native";
import { Title, Paragraph, Text, Divider } from "react-native-paper";
import { GRAPHQL_CLIENT, ErrorComponent, ROCKET_QUERY } from "../data/Constants";
import LoadingComponent from "./LoadingComponent";
import { RouteProp, ParamListBase } from "@react-navigation/native";

interface AboutRocketProps {
  route: RouteProp<ParamListBase, "AboutRocket"> & { params: { rocketId?: string } }; 
}

const RocketInfoComponent =  ({ route }: AboutRocketProps) => {

  const { rocketId = '' } = route.params || {};

    const DataComponent =  (rocket: Rocket) => (
        <View style={{ padding: 16 }}>
          <Title>Rocket {rocket.rocket.name}</Title>
          <Text>Country: {rocket.rocket.country}</Text>
          <Text>First flight: {rocket.rocket.first_flight}</Text>
          <Text>Description: </Text>
          <Paragraph>{rocket.rocket.description}</Paragraph>
          <Divider bold/>
          <View style={{ marginTop: 8 }}>
            <Text>Success Rate: {rocket.rocket.success_rate_pct}</Text>
            <Text>Stages: {rocket.rocket.stages}</Text>
            <Text>Cost per launch: {rocket.rocket.cost_per_launch}</Text>
          </View>
        </View>
      )

      
    const aboutRocket = GRAPHQL_CLIENT.createComponent<Rocket>(
      ROCKET_QUERY, 
        new Map([['id', rocketId]]),
        {
            loadingComponent: LoadingComponent,
            dataComponent: DataComponent,
            errorComponent: ErrorComponent
        }
    )
  
    return (
      <View>
        {aboutRocket}
      </View>
    );
  };
  
  export default RocketInfoComponent;
  