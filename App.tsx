import Header from './src/components/Header';
import React from 'react';
import {
  MD3LightTheme as DefaultTheme,
  PaperProvider,
} from 'react-native-paper';
import { registerRootComponent } from 'expo';

const lightTheme = {
  ...DefaultTheme,
  dark: false,
  colors: {
    "primary": "rgb(57, 42, 71)",
    "onPrimary": "rgb(255, 255, 255)",
    "primaryContainer": "rgb(119, 93, 145)",
    "onPrimaryContainer": "rgb(33, 0, 93)",
    "secondary": "rgb(0, 107, 94)",
    "onSecondary": "rgb(255, 255, 255)",
    "secondaryContainer": "rgb(118, 248, 226)",
    "onSecondaryContainer": "rgb(0, 32, 27)",
    "tertiary": "rgb(176, 46, 0)",
    "onTertiary": "rgb(255, 255, 255)",
    "tertiaryContainer": "rgb(255, 219, 209)",
    "onTertiaryContainer": "rgb(59, 9, 0)",
    "error": "rgb(186, 26, 26)",
    "onError": "rgb(255, 255, 255)",
    "errorContainer": "rgb(255, 218, 214)",
    "onErrorContainer": "rgb(65, 0, 2)",
    "background": "rgb(255, 251, 255)",
    "onBackground": "rgb(28, 27, 30)",
    "surface": "rgb(255, 251, 255)",
    "onSurface": "rgb(28, 27, 30)",
    "surfaceVariant": "rgb(230, 224, 236)",
    "onSurfaceVariant": "rgb(72, 69, 78)",
    "outline": "rgb(121, 117, 127)",
    "outlineVariant": "rgb(202, 196, 207)",
    "shadow": "rgb(0, 0, 0)",
    "scrim": "rgb(0, 0, 0)",
    "inverseSurface": "rgb(49, 48, 51)",
    "inverseOnSurface": "rgb(244, 239, 244)",
    "inversePrimary": "rgb(206, 189, 255)",
    "elevation": {
      "level0": "transparent",
      "level1": "rgb(247, 242, 252)",
      "level2": "rgb(243, 237, 250)",
      "level3": "rgb(238, 231, 248)",
      "level4": "rgb(237, 229, 247)",
      "level5": "rgb(234, 226, 246)"
    },
    "surfaceDisabled": "rgba(28, 27, 30, 0.12)",
    "onSurfaceDisabled": "rgba(28, 27, 30, 0.38)",
    "backdrop": "rgba(50, 47, 56, 0.4)"
  }
};

export default function App() {
  return (
    <PaperProvider theme={lightTheme}>
    <Header />
  </PaperProvider>
  );
}

registerRootComponent(App);
