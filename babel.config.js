module.exports = function(api) {
  api.cache(true);
  return {
    presets: ['babel-preset-expo'],
    overrides: [
      {
        test: ['./node_modules/lru-cache/*'],
        plugins: [
          [
            '@babel/plugin-proposal-private-methods',
            { loose: true }, // Set "loose" mode for this plugin
          ],
          // ["@babel/plugin-transform-private-property-in-object", { "loose": true }]
        ],
      },
    ],
  };
};
