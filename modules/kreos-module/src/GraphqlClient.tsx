import React, { ReactElement, useEffect, useState } from 'react';
import KreosModule from './KreosModule';

class GraphQlClient {
    private endpoint: string;
  
    constructor(apiEndpoint: string) {
      if (!this.isValidGraphQLUrl(apiEndpoint)) {
        throw new Error('Invalid GraphQL API endpoint.');
      }
  
      this.endpoint = apiEndpoint;
    }
  
    private isValidGraphQLUrl(url: string): boolean {
        return urlRegex.test(url);
    }

    public setupCache(query: string, maxEntities: number) {
      const cacheKey = this.endpoint + '_' + query
      KreosModule.setupCache(cacheKey, maxEntities);
    }

    public removeCache(query: string) {
      const cacheKey = this.endpoint + '_' + query
      KreosModule.removeCache(cacheKey);
    }

    private executeGraphQLQuery<T>(
      query: string,
      variables: Map<string, any> = new Map(),
      {
        onRequestStart,
        onRequestSuccess,
        onRequestError,
      }: {
        onRequestStart?: () => void;
        onRequestSuccess?: (data: any) => void;
        onRequestError?: (error: any) => void;
      } = {}
    ): Promise<any> {
      return new Promise((resolve, reject) => {
        const requestBody = JSON.stringify({query: query,variables: Object.fromEntries(variables)});
        const cacheKey = this.endpoint + '_' + query
        const cachedData = KreosModule.getCacheValue(cacheKey, requestBody);

        if (cachedData) {
          onRequestSuccess && onRequestSuccess(cachedData);
          resolve(cachedData);
          return;
        }
    
        const requestOptions: RequestInit = {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
          },
          body: requestBody,
        };
    
        onRequestStart && onRequestStart();
    
        fetch(this.endpoint, requestOptions)
          .then((response) => response.json())
          .then((data) => {
            if (data.errors) {
              const error = new Error(data.errors.map((error: { message: string }) => error.message).join('\n'));
              onRequestError && onRequestError(error);
              KreosModule.setCacheValue(cacheKey, requestBody, error)
              reject(error);
            } else {
              onRequestSuccess && onRequestSuccess(data.data);
              KreosModule.setCacheValue(cacheKey, requestBody, data.data)
              resolve(data.data);
            }
          })
          .catch((error) => {
            onRequestError && onRequestError(error);
            reject(error);
          });
      });
    }
  
    public query<T>(
      query: string,
      variables: Map<string, any> = new Map(),
      callbacks: {
        onRequestStart?: () => void;
        onRequestSuccess?: (data: any) => void;
        onRequestError?: (error: any) => void;
      } = {}
    ): Promise<any> {
      return new Promise((resolve, reject) => {
        try {
          this.executeGraphQLQuery(
            query,
            variables,
            {
              onRequestStart: () => {
                callbacks.onRequestStart && callbacks.onRequestStart();
              },
              onRequestSuccess: (data) => {
                callbacks.onRequestSuccess && callbacks.onRequestSuccess(data);
                resolve(data); 
              },
              onRequestError: (error) => {
                callbacks.onRequestError && callbacks.onRequestError(error);
                reject(error); 
              },
            }
          );
        } catch (error) {
          console.error('GraphQL error:', error);
          reject(error); 
        }
      });
    }
  
    public createComponent<T>(
      query: string,
      variables: Map<string, any> = new Map(),
      {
        loadingComponent,
        dataComponent,
        errorComponent,
      }: {
        loadingComponent: () => ReactElement;
        dataComponent: (data: T) => ReactElement;
        errorComponent: (error: any) => ReactElement;
      }
    ): ReactElement {
      const [componentStates, setComponentStates] = useState<ComponentStates>({
        loading: loadingComponent(),
        data: null,
        error: null,
      });
  
      useEffect(() => {
        const fetchData = async () => {
          try {
            const data = await this.query<T>(query, variables);
            setComponentStates({ data: dataComponent(data), loading: null, error: null });
          } catch (error) {
            setComponentStates({ error: errorComponent(error), loading: null, data: null });
          }
        };
  
        fetchData();
      }, [query, JSON.stringify(variables)]);
      return (
        <>
          {componentStates.loading}
          {componentStates.data}
          {componentStates.error}
        </>
      );
    }

    

    public useGraphQLQuery<T>(
      query: string,
      initialVars: Map<string, any> = new Map()
    ) {
      const [loading, setLoading] = useState<boolean>(true);
      const [data, setData] = useState<T | null>(null);
      const [error, setError] = useState<any | null>(null);
  
      const fetchData = async (vars: Map<string, any>) => {
        setLoading(true);
  
        try {
          const result = await this.query<T>(query, vars);
          setData(result);
        } catch (error) {
          setError(error);
        } finally {
          setLoading(false);
        }
      };
  
      useEffect(() => {
        let isMounted = true;
  
        fetchData(initialVars);
  
        return () => {
          isMounted = false;
        };
      }, [JSON.stringify(Array.from(initialVars.entries()))]);
  
      const execute = (newVars: Map<string, any>) => {
        fetchData(newVars);
      };
  
      return { loading, data, error, execute };
    }

  }

  export default GraphQlClient;


  interface ComponentStates {
    loading: ReactElement | null;
    data: ReactElement | null;
    error: ReactElement | null;
  }

  const urlRegex = /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-zA-Z0-9]+([-.]{1}[a-zA-Z0-9]+)*\.[a-zA-Z]{2,5}(:[0-9]{1,5})?(\/.*)?$/;