import * as React from 'react';

import { KreosModuleViewProps } from './KreosModule.types';

export default function KreosModuleView(props: KreosModuleViewProps) {
  return (
    <div>
      <span>{props.name}</span>
    </div>
  );
}
