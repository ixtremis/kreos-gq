export type ChangeEventPayload = {
  value: string;
};

export type KreosModuleViewProps = {
  name: string;
};
