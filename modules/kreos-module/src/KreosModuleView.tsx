import { requireNativeViewManager } from 'expo-modules-core';
import * as React from 'react';

import { KreosModuleViewProps } from './KreosModule.types';

const NativeView: React.ComponentType<KreosModuleViewProps> =
  requireNativeViewManager('KreosModule');

export default function KreosModuleView(props: KreosModuleViewProps) {
  return <NativeView {...props} />;
}
