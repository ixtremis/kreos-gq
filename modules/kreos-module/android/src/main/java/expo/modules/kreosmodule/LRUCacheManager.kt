package expo.modules.kreosmodule

import android.util.LruCache

class LRUCacheManager {

    val cacheMap: MutableMap<String, LruCache<String, Any>> = mutableMapOf()

    fun getCacheValue(query: String, request: String): Any? {
        val queryCache = cacheMap.get(query)
        if (queryCache != null) {
            return cacheMap.get(request)
        }
        return null
    }

    fun setCacheValue(query: String, request: String, response: Any) {
        val queryCache = cacheMap.get(query)
        if (queryCache != null) {
             queryCache.put(request, response)
        }
    }

    fun removeCache(query: String) {
        cacheMap.remove(query)
    }

    fun setupCache(query: String, maxSize: Int) {
        cacheMap[query] = LruCache<String, Any>(maxSize)
    }
}