package expo.modules.kreosmodule

import android.content.Context
import expo.modules.kotlin.AppContext
import expo.modules.kotlin.views.ExpoView

class KreosModuleView(context: Context, appContext: AppContext) : ExpoView(context, appContext)
