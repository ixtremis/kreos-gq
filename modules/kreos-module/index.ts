import { NativeModulesProxy, EventEmitter, Subscription } from 'expo-modules-core';

// Import the native module. On web, it will be resolved to KreosModule.web.ts
// and on native platforms to KreosModule.ts
import KreosModule from './src/KreosModule';
import KreosModuleView from './src/KreosModuleView';
import { ChangeEventPayload, KreosModuleViewProps } from './src/KreosModule.types';
import GraphQlClient from './src/GraphqlClient';

// Get the native constant value.
export const PI = KreosModule.PI;

export function hello(): string {
  return KreosModule.hello();
}

export function createGraphQlClient(url: string): GraphQlClient {
  return new GraphQlClient(url);
}

export async function setValueAsync(value: string) {
  return await KreosModule.setValueAsync(value);
}

const emitter = new EventEmitter(KreosModule ?? NativeModulesProxy.KreosModule);

export function addChangeListener(listener: (event: ChangeEventPayload) => void): Subscription {
  return emitter.addListener<ChangeEventPayload>('onChange', listener);
}

export { KreosModuleView, KreosModuleViewProps, ChangeEventPayload };
